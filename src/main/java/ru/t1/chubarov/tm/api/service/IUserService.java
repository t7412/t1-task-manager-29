package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws AbstractException;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role) throws AbstractException;

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @Nullable
    User findByLogin(@NotNull String login) throws AbstractException;

    @Nullable
    User findByEmail(@NotNull String email) throws AbstractException;

    @Nullable
    User removeOne(@NotNull User model) throws AbstractException;

    @NotNull
    User removeByLogin(@NotNull String login) throws AbstractException;

    @NotNull
    User removeByEmail(@NotNull String email) throws AbstractException;

    @Nullable
    User setPassword(@Nullable String id, @Nullable String password) throws AbstractException;

    @Nullable
    User updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName) throws AbstractException;

    void lockUserByLogin(@Nullable String login) throws AbstractException;

    void unlockUserByLogin(@Nullable String login) throws AbstractException;
}
