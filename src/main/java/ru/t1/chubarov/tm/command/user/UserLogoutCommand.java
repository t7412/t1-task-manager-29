package ru.t1.chubarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "logout";
    @NotNull
    private final String DESCRIPTION = "User logout.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
